module.exports = {
  people: getPeople()
};

function getPeople() {
  return [
    { id: 1, firstName: 'John', lastName: 'Papa', mobile: 212312315, roomType:'Standard Room Single', checkDate: 'May 4,1923', location: 'Florida' },
    { id: 2, firstName: 'Ward', lastName: 'Bell', mobile: 3123123581, roomType:'Standard Room Twin', checkDate: 'June 12,1967', location: 'California' },
    { id: 3, firstName: 'Colleen', lastName: 'Jones', mobile: 2585685681, roomType:'Dormitory Room', checkDate: 'May 52,1993', location: 'New York' },
    { id: 4, firstName: 'Madelyn', lastName: 'Green', mobile: 15685685688, roomType:'Standard Room Single', checkDate: 'March 21,1983', location: 'North Dakota' },
    { id: 5, firstName: 'Ella', lastName: 'Jobs', mobile: 185675456, roomType:'Standard Room Twin', checkDate:'February 27,1991', location: 'South Dakota' },
    { id: 6, firstName: 'Landon', lastName: 'Gates', mobile: 145645641, roomType:'Dormitory Room',checkDate:'September 28,1996', location: 'South Carolina' },
    { id: 7, firstName: 'Haley', lastName: 'Guthrie', mobile: 34564565, roomType:'Standard Room Twin',checkDate:'April 12,1987', location: 'Wyoming' },
    { id: 8, firstName: 'Aaron', lastName: 'Jinglehiemer', mobile: 24564562, roomType:'Cabana Room',checkDate: 'May 31,1970', location: 'Utah' },
    { id: 9, firstName: 'Aaron2', lastName: 'Jinglehiemer2', mobile: 123123, roomType:'Standard Room Single',checkDate: 'December 31,1967', location: 'Philippines' }
];
}
