(function () {
  'use strict';

  angular.module('app.directives')

  .directive('birthdayDirective', function () {
    return {
      restrict: 'AE',
      scope: true,
      controller: function () {
        var vm = this;
        vm.curDate = new Date();
        vm.minAge = new Date(vm.curDate.getFullYear() - 18, vm.curDate.getMonth(), vm.curDate.getDate());
      },
      controllerAs: 'bdayCtrl',

      templateUrl: '/src/client/app/directives/birthday/birthday.html'

    }
  });

})();
