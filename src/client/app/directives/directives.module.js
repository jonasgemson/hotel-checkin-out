(function() {
  'use strict';

  angular.module('app.directives', [
    'app.core',
    'app.widgets',
    'ngMessages'
  ]);
})();
