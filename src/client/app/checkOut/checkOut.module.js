(function() {
  'use strict';

  angular.module('app.checkOut', [
    'app.core',
    'app.widgets'
  ]);

})();
