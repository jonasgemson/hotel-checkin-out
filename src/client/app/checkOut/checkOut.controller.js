(function() {
  'use strict';

  angular
    .module('app.checkOut')
    .controller('checkOutController', checkOutController);

  checkOutController.$inject = ['$q', 'dataservice', 'logger','$scope'];
  /* @ngInject */
  function checkOutController($q, dataservice, logger,$scope) {
    var vm = this;
    vm.messageCount = 0;
    vm.people = [];
    vm.title = 'Dashboard';
    console.log('this dashboard Successfull')
    activate();

    function activate() {
      var promises = [getMessageCount(), getPeople()];
      return $q.all(promises).then(function() {
        logger.info('Activated Dashboard View');
      });
    }

    function getMessageCount() {
      return dataservice.getMessageCount().then(function(data) {
        vm.messageCount = data;
        return vm.messageCount;
      });
    }

    function getPeople() {
      return dataservice.getPeople().then(function(data) {
        vm.people = data;
        return vm.people;
      });
    }





  }
})();
