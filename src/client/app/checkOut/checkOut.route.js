(function() {
  'use strict';

  angular
    .module('app.checkOut')
    .run(appRun);

  appRun.$inject = ['routerHelper'];
  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'checkOut',
        config: {
          url: '/checkOut',
          templateUrl: 'app/checkOut/checkOut.html',
          controller: 'checkOutController',
          controllerAs: 'vm',
          title: 'Check Out',
          settings: {
            nav: 5,
            content: '<i class="fa fa-lock"></i> Check Out'
          }
        }
      }
    ];
  }
})();
