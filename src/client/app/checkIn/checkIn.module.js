(function() {
  'use strict';

  angular.module('app.checkIn', [
    'app.core',
    'app.widgets'
  ]);

})();
