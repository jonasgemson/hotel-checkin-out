(function() {
  'use strict';

  angular
    .module('app.checkIn')
    .run(appRun);

  appRun.$inject = ['routerHelper'];
  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'checkIn',
        config: {
          url: '/checkIn',
          templateUrl: 'app/checkIn/checkIn.html',
          controller: 'checkInController',
          controllerAs: 'vm',
          title: 'Check In',
          settings: {
            nav: 4,
            content: '<i class="fa fa-lock"></i> Check In'
          }
        }
      }
    ];
  }
})();
