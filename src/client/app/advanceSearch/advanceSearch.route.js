(function() {
  'use strict';

  angular
    .module('app.advanceSearch')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'advanceSearch',
        config: {
          url: '/advanceSearch',
          templateUrl: 'app/advanceSearch/advanceSearch.html',
          controller: 'advanceSearchController',
          controllerAs: 'vms',
          title: 'Advance Search',
          settings: {
            nav: 6,
            content: '<i class="fa fa-dashboard"></i> advanceSearch'
          }
        }
      }
    ];
  }
})();
