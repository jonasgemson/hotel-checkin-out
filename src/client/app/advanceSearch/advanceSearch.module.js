(function() {
  'use strict';

  angular.module('app.advanceSearch', [
    'app.core',
    'app.widgets'
  ]);
})();
