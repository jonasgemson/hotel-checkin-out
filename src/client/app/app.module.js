(function() {
  'use strict';

  angular.module('app', [
    'app.core',
    'app.widgets',
    'app.admin',
    'app.dashboard',
    'app.layout',
    'app.login',
    'app.directives',
    'app.advanceSearch',
    'app.checkIn',
    'app.checkOut'
  ]);

})();
